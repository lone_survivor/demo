const constants = require('./constant.js');

module.exports = {
    ci: {
        collect: {
            url: ["https://www.devairasia.com/en/gb"],
            extends: 'lighthouse:default',
            settings: {
                formFactor: "desktop",
                throttling: constants.throttling.desktopDense4G,
                screenEmulation: constants.screenEmulationMetrics.desktop,
                emulatedUserAgent: constants.userAgents.desktop,
                chromeFlags: "--no-sandbox",
            },
            numberOfRuns: 1
        },
        upload: {
            target: "temporary-public-storage",
        },
    },
};
